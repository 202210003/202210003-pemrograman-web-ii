<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
 <div class="judul">
        <h2>Buat Account Baru!</h2>
        <h4>Sign Up Form</h4>
    </div>
    <div class="form-signUp">
        <form action="">
            <label for="fname">First Name :</label>
            <input type="text" name="fname" />
        <br/>
            <label for="lname">Last Name :</label>
            <input type="text" name="lname" />
    </div>
    <br />
    <div class="gender">
        <p>Gender</p>
            <li>
                <input type="radio" />
                <label for="Male">Male</label>
            </li>
            <li>
                <input type="radio" />
                <label for="Female">Female</label>
            </li>
            <li>
                <input type="radio" />
                <label for="Other">Other</label>
            </li>
    </div>
    <br />
    <div class="country">
        <p>Nationality</p>
            <select name="lang" id="">
                <option value="indo">Indonesian</option>
                <option value="indo">English</option>
                <option value="indo">Other</option>
            </select>
    </div>
    <div class="lang">
        <p>Language Spoken</p>
            <li>
                <input type="radio" />
                <label for="indo">Bahasa Indonesia</label>
            </li>
            <li>
                <input type="radio" />
                <label for="eng">English</label>
            </li>
            <li>
                <input type="radio" />
                <label for="Other">Other</label>
            </li>
    </div>
    <br />
    <div class="bio">
        <p>Bio :</p>
        <textarea> </textarea>
    </div>
    <br />
    <div class="buttonSignUp">
        <button><a href="/berhasil">Sign Up</a></button>
    </div>
	</form>
</body>
</html>